# coding: utf-8

from console import *
from time import *

def try_remove(self: list, value):
    try:
        self.remove(value)
    except:
        pass

class Sudoku:
    def __init__(self, speed):
        self.grid = [[[0, [], False] for i in range(9)] for j in range(9)]
        self.speed = speed

    def display(self):
        for i, line in enumerate(self.grid):
            for j, val in enumerate(line):
                self.set_cursor_in_grid(j,i)
                print(str_red(val[0]) if val[2] else val[0])

    def set_cursor_in_grid(self, x, y):
        set_cursor_at((x * 2) + 2 + int(x / 3) * 2, y + 1 + int(y / 3))

    def solve(self):
        compt = 0
        move_forward = True
        while compt < 81:
            x = int(compt%9)
            y = int(compt/9)
            if self.grid[y][x][2]:
                compt += 1 if move_forward else -1
            else:
                if(move_forward):
                    values = self.get_available_numbers(x, y)
                    if(len(values) == 0):
                        self.grid[y][x][0] = 0
                        compt -= 1
                        move_forward = False
                    else:
                        self.grid[y][x][0] = values[0]
                        compt += 1
                else:
                    try_remove(self.grid[y][x][1], self.grid[y][x][0])
                    if(len(self.grid[y][x][1]) == 0):
                        self.grid[y][x][0] = 0
                        compt -= 1
                    else:
                        self.grid[y][x][0] = self.grid[y][x][1][0]
                        compt += 1
                        move_forward = True
                if self.speed > 1:
                    self.set_cursor_in_grid(x,y)
                    print(self.grid[y][x][0])
                    if self.speed > 2:
                        sleep(0.5)
        self.display()
        print('\n')
                    

    def get_available_numbers(self, x, y):
        available_numbers = [1,2,3,4,5,6,7,8,9]
        for i in range(9):
            try_remove(available_numbers, self.grid[i][x][0])
        for j in range(9):
            try_remove(available_numbers, self.grid[y][j][0])
        for k in range(9):
            try_remove(available_numbers, self.grid[(int(y/3) * 3) + int(k / 3)][(int(x/3) * 3) + int(k % 3)][0])

        self.grid[y][x][1] = available_numbers
        return available_numbers

    def display_grid(self):
        print('┌───────┬───────┬───────┐')
        print('│       │       │       │')
        print('│       │       │       │')
        print('│       │       │       │')
        print('├───────┼───────┼───────┤')
        print('│       │       │       │')
        print('│       │       │       │')
        print('│       │       │       │')
        print('├───────┼───────┼───────┤')
        print('│       │       │       │')
        print('│       │       │       │')
        print('│       │       │       │')
        print('└───────┴───────┴───────┘')

    def encode_grid(self):
        x, y = 0, 0
        while True:
            self.set_cursor_in_grid(x,y)
            key = get_ch()
            if key == b'M' and x < 8:
                x += 1
            elif key == b'K' and x > 0:
                x -= 1
            elif key == b'H' and y > 0:
                y -= 1 
            elif key == b'P' and y < 8:
                y += 1
            elif key == b'\x08':
                self.grid[y][x][0] = 0
                self.grid[y][x][2] = False
                print(' ')
            elif key == b'\r':
                return
            else:
                try:
                    val = int(key)
                    values = self.get_available_numbers(x,y)
                    if val in values:
                        self.grid[y][x][0] = val
                        self.grid[y][x][2] = True
                        print(val)
                except:
                    pass

menu1 = Menu(0,0,'Mode', ['Fast', 'Normal', 'Slow'])
menu2 = Menu(0,0,'Choose an option', ['New', 'Quit'])
continue_option = 1
while continue_option != 2:
    clear()
    speed = menu1.display()
    clear()
    sudo = Sudoku(speed)
    sudo.display_grid()
    sudo.encode_grid()
    sudo.display()
    get_ch()
    sudo.solve()
    input('Press any key to continue')
    clear()
    continue_option = menu2.display()

