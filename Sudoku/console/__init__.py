from ctypes import *
from os import system, name
from msvcrt import getch
from time import sleep
from threading import Thread

 
STD_OUTPUT_HANDLE = -11
 
class COORD(Structure):
    pass
 
COORD._fields_ = [("X", c_short), ("Y", c_short)]
 
def print_at(c, r, s):
    h = windll.kernel32.GetStdHandle(STD_OUTPUT_HANDLE)
    windll.kernel32.SetConsoleCursorPosition(h, COORD(c, r))
 
    c = str(s).encode("windows-1252")
    windll.kernel32.WriteConsoleA(h, c_char_p(c), len(c), None, None)

def set_cursor_at(c, r):
    h = windll.kernel32.GetStdHandle(STD_OUTPUT_HANDLE)
    windll.kernel32.SetConsoleCursorPosition(h, COORD(c, r))

def clear():
    if name == "nt":
        system("cls")
    else:
        system("clear")

def get_ch():
    return getch()

def question(x, y, text):
    for i, char in enumerate(text):
        sleep(0.05)
        print_at(x + i, y, char)
    print('\n')
    return input()

def str_red(value):
    return '\033[91m'+str(value)+'\033[0m'

def str_green(value):
    return '\033[92m'+str(value)+'\033[0m'

def str_blue(value):
    return '\033[94m'+str(value)+'\033[0m'

def str_yellow(value):
    return '\033[93m'+str(value)+'\033[0m'

def str_purple(value):
    return '\033[95m'+str(value)+'\033[0m'

def str_underline(value):
    return '\033[4m'+str(value)+'\033[0m'

def str_white(value):
    return '\033[1m'+str(value)+'\033[0m'

class Menu():
    class MenuThread(Thread):
        def __init__(self, menu):
            Thread.__init__(self)
            self.menu = menu
        def run(self):
            while self.menu.on:
                choix = self.menu.choix
                text = str(choix + self.menu.start_index) + '. ' + self.menu.titles[self.menu.choix]
                blank = ''.join([' ' for i in range(len(text))])
                print_at(self.menu.x, self.menu.y + choix + 1, text)
                sleep(0.25)
                print_at(self.menu.x, self.menu.y + choix + 1, blank)
                sleep(0.25)
                print_at(self.menu.x, self.menu.y + choix + 1, text)

    def __init__(self, x, y, title, titles, start_index = 1):
        self.x = x
        self.y = y
        self.title = title
        self.titles = titles
        self.choix = 0
        self.on = False
        self.start_index = start_index

    def display(self):
        self.on = True
        set_cursor_at(self.x, self.y)
        print(str_underline(self.title))
        for i, text in enumerate(self.titles):
            print_at(self.x, self.y + i + 1, str(i + self.start_index) + '. '  + text)

        thread = self.MenuThread(self)
        thread.start()
        while True:
            key = get_ch()
            if key == b'\r':
                self.on = False
                thread.join()
                return self.choix + self.start_index
            if key == b'P':
                self.choix = (self.choix + 1) % len(self.titles)
            if key == b'H':
                self.choix = (self.choix - 1 + len(self.titles)) % len(self.titles)

    